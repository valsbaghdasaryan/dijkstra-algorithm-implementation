package com.company.BinaryHeap;

import com.company.Node;

/**
 * Created by Vardan on 04/11/2016.
 */
public class BinaryHeap {

    private Vertex[] vertices;
    private int length;

    public BinaryHeap()
    {
        length = 0;
    }

    private void increaseVerticesLength(){
        if(vertices == null){
            length = 1;
            vertices = new Vertex[length];
        }
        else {
            Vertex[] a = new Vertex[length +1];
            for(int i = 0; i < length; i++)
            {
                a[i] = vertices[i];

            }
            vertices = a;
            length ++;
        }
    }

    private void decreaseVerticesLength(){
        if(vertices == null){
            return;
        }
        else {
            Vertex[] a = new Vertex[length -1];
            for(int i = 0; i < length-1; i++)
            {
                a[i] = vertices[i];

            }
            vertices = a;
            length --;
        }
    }

    public void add(Vertex v)
    {
        increaseVerticesLength();
        vertices[length-1] = v;
        shiftUp();
    }

    public void removeRoot(){
        remove(0);
    }

    public void remove(int index){
        vertices[index] = vertices[length - 1];
        decreaseVerticesLength();
        shiftDown(index);
    }

    public void increaseRootValue(double value){
        vertices[0].setId(value);
        shiftDown(0);
    }

    public Vertex getRoot(){
        return vertices[0];
    }

    public void setValue(Node node, double value)
    {
        for(int i = 0; i < length; i ++)
        {
            if(vertices[i].getNode().getNodeIndex() == node.getNodeIndex()){
                vertices[i].setId(value);
                if(hasParent(i) && vertices[parentIndex(i)].getId() > vertices[i].getId()){
                    shiftUp(i);
                }else {
                    shiftDown(i);
                }
                return;
            }
        }

//        throw new Error("BinaryHeap : node doesnt exist");

    }

    private boolean hasParent(int current){
        return  current != 0;
    }

    private int parentIndex(int current){
        return (current - 1)/2;
    }

    public boolean isEmpty(){
        return length == 0;
    }

    public void shiftDown(int current){
//        if(isLeaf(current)){
//            System.out.println("is leaf : " + current);
//            return;
//        }

        int leftChild = leftChildIndex(current);
        int rightChild = leftChild + 1;

        if(hasRight(current))
        {
            if(vertices[leftChild].getId() < vertices[current].getId())
            {
                if(vertices[leftChild].getId() < vertices[rightChild].getId()){
                    swap(current, leftChild);
                    shiftDown(leftChild);
                }else {
                    swap(current, rightChild);
                    shiftDown(rightChild);

                }
            }else if(vertices[rightChild].getId() < vertices[current].getId()) {
                swap(current, rightChild);
                shiftDown(rightChild);
            }
        }else if(hasLeft(current))
        {
            if(vertices[leftChild].getId() < vertices[current].getId())
            {
                swap(leftChild, current);
                shiftDown(current);
            }
        }
    }

    private boolean hasLeft(int current){
        return leftChildIndex(current ) < length;
    }

    private boolean hasRight(int current){
        return rightChildIndex(current ) < length;
    }

    private int leftChildIndex(int current){
        return  2*current + 1;
    }
    private int rightChildIndex(int current){
        return  2*current + 2;
    }

    private boolean isLeaf(int index){

//        int currentHeight = (index == 0) ? 0 : (int)(Math.log(index)/Math.log(2));
        int currentHeight = (int)(Math.log(index+1)/Math.log(2));
        int leafHeight = (int)(Math.log(length)/Math.log(2));
        return currentHeight == leafHeight;
    }


    public void shiftUp(){
        int currentIndex = length-1;
        shiftUp(currentIndex);
    }

    public void shiftUp(int currentIndex)
    {
        if(currentIndex == 0)
        {
            return;
        }
        int parentIndex = (currentIndex - 1)/2;
        if(vertices[currentIndex].getId() < vertices[parentIndex].getId()){
            swap(currentIndex, parentIndex);
            shiftUp(parentIndex);
        }
    }

    public void swap(int index1, int index2)
    {
        Vertex v1 = vertices[index1];
        vertices[index1] = vertices[index2];
        vertices[index2] = v1;
    }

    public void printHeap()
    {
        for(int i = 0; i < length; i++)
        {
            System.out.print(vertices[i].toString() + " ");
        }
        System.out.println("\n\n");

    }

}
