package com.company.BinaryHeap;

import com.company.Node;

/**
 * Created by Vardan on 04/11/2016.
 */
public class Vertex {
    private Node node;

    public Node getNode(){return  node;}

    public double getId(){
        return node.getDistanceFromSource();
    }

    public void setId(double value){

        node.setDistanceFromSource(value);
    }
    public Vertex(Node vertexID)
    {
        node = vertexID;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Vertex)
        {
            return  (((Vertex) obj).getId() == node.getDistanceFromSource());
        }
        else return  false;
    }

    @Override
    public String toString() {
        return String.valueOf(node.getDistanceFromSource());
    }
}
