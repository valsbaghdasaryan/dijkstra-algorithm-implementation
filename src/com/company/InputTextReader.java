package com.company;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Vardan on 04/11/2016.
 */
public class InputTextReader {

    //The format of graph files is the following : the
    //first line gives the number of nodes and the number of links, then all nodes are listed
    //with a id number, and two coordinates, then the list of links between node ids is given.
    //A simple example with 3 nodes and 2 links :
    //3 2
    //0 100 1000
    //1 5 200
    //2 10 100

    //0 1
    //1 2


    private final String PATH = "Assets/";

    private final String MAP5x5 = "map5x5.txt";
    private final String BITMAP = "bigmap.txt";
    private final int ID_MAP5x5 = 1;
    private final int ID_BIGMAP = 2;

    private boolean isInitialized;
    private Node[] nodes;
    private Edge[] edges;

    private int mapId;
    private String path;

    public Node[] getNodes()
    {
        return nodes;
    }

    public InputTextReader(int id )
    {
        mapId = id;
        switch (id){
            case ID_MAP5x5:
                path = PATH + MAP5x5;
                break;
            case ID_BIGMAP:
                path = PATH + BITMAP;
                break;
            default:
                path = PATH + MAP5x5;
                return;
        }
    }

    public void readFromSource()
    {
        try{
            File file = new File(path);
            FileReader reader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(reader);
            String line = bReader.readLine();
            String[] nums = line.split(" ");

            int i;
            nodes = new Node[Integer.parseInt(nums[0])];
            edges = new Edge[Integer.parseInt(nums[1])];

            String nodeLine;
            String linkLine;

            String[] nodeArray;
            String[] linkArray;


            // Reading nodes with their coordinates
            for(i = 0; i < nodes.length; i ++)
            {
                if((nodeLine = bReader.readLine()) != null){
                    nodeArray = nodeLine.split("( )+");
                    addNode(i, nodeArray);

                }else {
                    throw new Error("node line doesn't exist");
                }
            }

            // Reading links with
            for(i = 0; i < edges.length; i ++)
            {
                if((linkLine = bReader.readLine()) != null){
                    linkArray = linkLine.split("( )+");
                    addEdge(i, linkArray);
                }else {
                    throw new Error("node line doesn't exist");
                }
            }
            reader.close();
            bReader.close();

            isInitialized = true;
        }
        catch (IOException error)
        {
            error.printStackTrace();
        }
    }



    private void addNode(int index, String[] nodeArray)
    {
        // TODO need to change
        int startIndex;
        if(mapId == 1)
        {
            startIndex = 1;
        }else {
            startIndex = 2;
        }

        int xCoord = Integer.parseInt(nodeArray[startIndex]);
        int yCoord = Integer.parseInt(nodeArray[startIndex + 1]);
        nodes[index] = (new Node(index, xCoord, yCoord));

    }

    private void addEdge(int index, String[] vertices)
    {
        int si;

        // TODO need to change
        if(mapId == 1)
        {
            si = 0;
        }
        else {
            si = 1;
        }
        int nodeFrom = Integer.parseInt(vertices[si]);
        int nodeTo = Integer.parseInt(vertices[si + 1]);

        edges[index] = (new Edge(nodes[nodeFrom],nodes[nodeTo]));
    }

    public boolean isReady() {
        return isInitialized;
    }

    private void print(String message)
    {
        System.out.println(message);
    }

    private void print(int message){
        System.out.println(message);
    }
}
