package com.company;

/**
 * Created by Vardan on 04/11/2016.
 */
public class Edge {
    private Node nodeFrom;
    private Node nodeTo;
    private double distance = Integer.MAX_VALUE;

    public Edge(Node nodeFrom, Node nodeTo)
    {
        this.nodeFrom = nodeFrom;
        this.nodeTo = nodeTo;
        this.distance = this.nodeTo.getDistance(this.nodeFrom);

        this.nodeFrom.AddEdge(this);
//        this.nodeTo.AddEdge(this);
    }


    public Node getNodeIndexFrom() {
        return nodeFrom;
    }
    public Node getNodeTo() {
        return nodeTo;
    }
    public double getDistance() {
        return distance;
    }

}
