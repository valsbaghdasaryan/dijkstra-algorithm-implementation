package com.company;

import java.util.ArrayList;

/**
 * Created by Vardan on 04/11/2016.
 */
public class Node {

    private int nodeIndex;
    private ArrayList<Edge> edges;
    private int xCoord;
    private int yCoord;
    private double distanceFromSource = Integer.MAX_VALUE;
    private Node[] nodesPathFromSource;
    private int edgeCount = 0;

    private boolean isVisited;

    public void setNodesPathFromSource(Node[] nodes){
        nodesPathFromSource = nodes;
    }
    public Node[] getNodesPathFromSource(){
        if(nodesPathFromSource == null)
            nodesPathFromSource = new Node[0];
        return nodesPathFromSource;
    }

    public void addNodePath(Node n)
    {
        if(nodesPathFromSource == null)
        {
            nodesPathFromSource = new Node[1];
            nodesPathFromSource[0] = n;
        }
        else {
            int length = nodesPathFromSource.length;
            Node[] a = new Node[length + 1];

            for(int i = 0; i < length; i++)
            {
                a[i] = nodesPathFromSource[i];
            }
            a[length] = n;
            nodesPathFromSource = a;
        }
    }

    public Node(int id, int xCord, int yCord){
        this.nodeIndex = id;
        this.xCoord = xCord;
        this.yCoord = yCord;
        this.isVisited = false;

        edges = new ArrayList<>();

    }

    public void AddEdge(Edge edge){
        edges.add(edge);
        edgeCount ++;
    }



    public int getNodeIndex() {
        return nodeIndex;
    }

    public void setNodeIndex(int nodeIndex) {
        this.nodeIndex = nodeIndex;
    }

    public int getyCoord() {
        return yCoord;
    }

    public int getxCoord() {
        return xCoord;
    }

    public ArrayList<Edge> getEdges(){
        return  edges;

    }

    public Edge getEdge(int index)
    {
        return  edges.get(index);
    }
    public int getEdgesCount(){
        return edgeCount;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    public double getDistance(Node fromNode){
        return Math.sqrt(Math.pow(this.xCoord - fromNode.getxCoord(), 2) + Math.pow(this.yCoord - fromNode.getyCoord(), 2));
    }

    public boolean isVisited() {
        return isVisited;
    }

    public double getDistanceFromSource() {
        return distanceFromSource;
    }

    public void setDistanceFromSource(double distanceFromSource) {
        this.distanceFromSource = distanceFromSource;
    }

    public void addDistanceFromSource(double value){
        this.distanceFromSource += value;
    }
}
