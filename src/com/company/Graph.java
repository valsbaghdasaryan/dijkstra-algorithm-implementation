package com.company;

import com.company.BinaryHeap.BinaryHeap;
import com.company.BinaryHeap.Vertex;

import java.sql.Time;
import java.util.Date;

/**
 * Created by Vardan on 04/11/2016.
 */
public class Graph {

    private Node[] nodes;
    private BinaryHeap minHeap;
    private long startTime;
    private long endTime;
    private int complexity = 0;
    private int source;


    public Graph(Node[] n, int source) {
        this.nodes = n;
        this.source = 0;

        minHeap = new BinaryHeap();

        nodes[source].setDistanceFromSource(0);// dist[source] = 0
        for (int i = 0; i < nodes.length; i++) {
            minHeap.add(new Vertex(nodes[i]));
        }
    }

    public long getTime()
    {
        return endTime - startTime;
    }

    public void dijkstra(){
        startTime = System.currentTimeMillis();
        while (true)
        {
            Node currentNode = minHeap.getRoot().getNode();
            if(currentNode.isVisited()){
                throw new Error("node should be not visisted");
            }

            getDistances(currentNode);

            currentNode.setVisited(true);
            minHeap.removeRoot();

            if(minHeap.isEmpty()){
                endTime = System.currentTimeMillis();
                return;
            }
        }

    }


    private void setDistanceFromSource(int index, double value)
    {
        minHeap.setValue(nodes[index], value);
    }
    private void setDistanceFromSource(Node node, double value)
    {
        setDistanceFromSource(node.getNodeIndex(), value);
    }
    private void getDistances(Node node)
    {
        System.out.println(complexity++);
        for(int i = 0; i < node.getEdgesCount(); i++)
        {
            Edge edge = node.getEdge(i);
            Node nodeTo = edge.getNodeTo();
            double dis = node.getDistanceFromSource() + edge.getDistance();
            if(dis < nodeTo.getDistanceFromSource())
            {
                this.setDistanceFromSource(nodeTo, dis);
                nodeTo.setNodesPathFromSource(node.getNodesPathFromSource());
                nodeTo.addNodePath(node);
            }
        }

//        node.setVisited(true);
//        minHeap.removeRoot();
//
//        if(minHeap.isEmpty()){
//            return;
//        }
//
//        Node n = minHeap.getRoot().getNode();
//        if(n.isVisited()){
//            throw new Error("node should be not visisted");
//        }
//        getDistances(n);

    }

    public void print()
    {
        for(int i = 0; i < nodes.length; i ++){
            System.out.println("node " + i + " distance " + nodes[i].getDistanceFromSource() );
        }
    }

}
