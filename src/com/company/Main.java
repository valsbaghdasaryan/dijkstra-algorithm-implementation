package com.company;
public class Main {

    public static void main(String[] args) {

        int mapId = 1;
        if(args.length > 0){
            mapId = Integer.parseInt(args[0]);
        }

        InputTextReader reader = new InputTextReader(mapId);
        reader.readFromSource();

        if(!reader.isReady()){
            throw new Error("data is not ready");
        }

        Graph g = new Graph(reader.getNodes(), 5);
        g.dijkstra();
        g.print();



        // For testing
        Node[] n = reader.getNodes()[22].getNodesPathFromSource();
        for(int i = 0; i < n.length; i++)
        {
            System.out.println( "Node id: " + n[i].getNodeIndex() + ", distance from source: " + n[i].getDistanceFromSource());
        }

        System.out.println("time : " + g.getTime());

    }


}
